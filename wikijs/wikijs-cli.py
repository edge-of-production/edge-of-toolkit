import argparse
import requests
from bs4 import BeautifulSoup


class WikiTools:
  """Toolkit for wikijs contents composing
  Parameters:
  ----
  url     (str) : url which is parsed by this tool (Default: None)
  title   (str) : title which is extracted by this tools (Default: None) 
  charset (str) : Character-set only use when garbled character happens 
  """


  def __init__(self, url=None):
    """Initialize Instance variables
    """
    self.url = url
    self.response_body = ''
    self.title = ''
    self.charset = None

  def exec_request(self, url=None, charset=None):
    """Execute http(s) request
    Parameters:
    ----
    url (str)     : url requested to (Default: None)
    charset (str) : encoding of response text to avoid garbled character. (Default: None)
    
    Return:
    ----
    response_body (str): http(s) response body; return None if url is not set.
    ""

    # if url arg is given, set into instance vars.
    self.url = url if url is not None else self.url

    # execute http(s) request when url is given.
    res = requests.get(self.url) if self.url is not None else None
    # set encoding if-when encoding is given.
    res.encoding = None if charset is None else charset
    # Return body of http(s) response
    if res is not None and res.status_code == 200:
      self.response_body = res.text
      return res.text
    else:
      return None
    
  def parse_title(self, url=None):
    """Extract Title from http response body and return markdown formatted url and title.
    this method calls `exec_request` to get http response.
    Parameters:
    ----
    url (str)  : url requested to (Default: None)

    Return
    ----
    (str) markdown formatted url and tile of url.
    """
    self.url = url if url is not None else self.url

    if self.exec_request(self.url) is not None:
      soup = BeautifulSoup(self.response_body, 'html.parser')
      self.title = soup.find(['title']).text
      return '[{}]({})'.format(self.title, self.url)  

if __name__ == "__main__":  
  # Argparser
  parser = argparse.ArgumentParser()
  parser.add_argument('action', choices=['oneline', 'markdown'], help='retriver action')
  parser.add_argument('url', help='url retrieved', type=str)
  parser.add_argument('--encoding', help='Encoding Charset')
  args = parser.parse_args()


  tools = WikiTools()
  if args.action == 'oneline':
    print(tools.parse_title(args.url))
